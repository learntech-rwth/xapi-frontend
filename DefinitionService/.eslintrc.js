module.exports = {
  parser: "@typescript-eslint/parser",
  parserOptions: {
    project: "./tsconfig.json",
    tsconfigRootDir: __dirname,
    impliedStrict: true,
    ecmaVersion: 6,
  },
  extends: ["../common/.eslintbase.js"],
};
