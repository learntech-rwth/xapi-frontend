import {
  NextFunction,
  Request,
  Response,
  Router,
  RouterOptions,
} from "express";
import {
  DefinitionAccessor,
  FilePath,
  NotFoundException,
} from "../accessor/AccessorTypes";
import { isArray, isEmpty, isString } from "lodash";
import { log } from "../logger/logger";
import { MongoOptions } from "../accessor/MongoAccessor";
import { isStringArray } from "../utils/Util";
import cors from "cors";
import expressMongoSanitize from "express-mongo-sanitize";

export class DefinitionRouter {
  readonly router: Router;

  constructor(
    private accessor: DefinitionAccessor<FilePath, MongoOptions>,
    options?: RouterOptions,
  ) {
    this.router = Router(options);
    this.router.use(cors());
    this.router.use(expressMongoSanitize());

    const fileNamePattern = /^(\/[\w_-]+)*(\/|.json)?$/;

    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    this.router.get(
      fileNamePattern,
      async (
        req: Request,
        res: Response,
        next: NextFunction,
      ): Promise<void> => {
        const options: MongoOptions = {};
        const fields = req.query.fields;
        options.fields = this.parseQueryParams(fields);
        try {
          const rpath = this.removeLeadingSlash(req.path);
          log.debug(rpath);
          const result = await this.accessor.read(req, rpath, options);
          res.send(result);
          log.debug({ Sent: result });
        } catch (error) {
          if (!(error instanceof NotFoundException)) {
            return next(error);
          }
          req.log.info(error, "Not Found");
          res
            .status(error.status)
            .send({ message: error.message, details: error.details });
        }
      },
    );
  }

  private parseQueryParams(fields: unknown): string[] {
    if (isArray(fields) && isStringArray(fields)) {
      return fields;
    }
    if (isString(fields)) {
      return fields.split(",");
    }
  }

  removeLeadingSlash(target: string): string {
    if (!isEmpty(target) && target.substr(0, 1) === "/") {
      return target.substr(1);
    }
    return target;
  }
}
