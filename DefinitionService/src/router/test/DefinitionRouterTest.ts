import * as request from "request";
import { DefinitionRouter } from "../DefinitionRouter";
import { GenericServer } from "../../server/GenericServer";
import { first } from "lodash";
import { MongoConnector } from "../../mongo/MongoConnector";
import { buildTestDataBase } from "../../mongo/buildDatabase";
import { MongoAccessor } from "../../accessor/MongoAccessor";
import {
  definitionCollection,
  definitionStructureCollection,
} from "../../mongo/MongoConstants";
import { Definition } from "../../types/DefinitionTypes";

describe("[DefinitionRouter]", () => {
  let mongo: MongoConnector;
  let server: GenericServer;

  beforeAll(async () => {
    mongo = await buildTestDataBase(
      "src/accessor/test/fixture/definitions",
      "https://xapistinkt.com/definitions/",
    );
    const db = mongo.db("test");
    const collection = db.collection(definitionCollection);
    const structure = db.collection(definitionStructureCollection);
    const accessor = new MongoAccessor(collection, structure);
    const definitionRouter = new DefinitionRouter(accessor);

    server = new GenericServer({
      middlewareHandler: (app): void => {
        app.use("/definitions", definitionRouter.router);
      },
      mongo,
      port: 3333,
    });
    return server.started;
  });

  afterAll(() => {
    // closes mongoconnection too
    server.close();
  });

  it("should deliver a definition array with one definition on specific file path", (done) => {
    const endpoint = "http://localhost:3333/definitions/game/generics/game";
    request.get(endpoint, function (error, response) {
      expect(response.statusCode).toEqual(200);
      const { definitions } = JSON.parse(response.body);
      const definition: Definition = first(definitions);
      // eslint-disable-next-line @typescript-eslint/no-var-requires
      expect(definition).toEqual({
        id: "game/generics/game",
        name: { "en-US": "generic game" },
        description: {
          "en-US": "Represents a generic game or competition of any kind.",
        },
        iri: "https://xapistinkt.com/definitions/game/generics/game",
      });
      done();
    });
  });

  it("should deliver the specified fields for requested definition", (done) => {
    const endpoint =
      "http://localhost:3333/definitions/game/game?fields=id,name,versions,history";
    request.get(endpoint, (error, response) => {
      expect(response.statusCode).toEqual(200);
      const { definitions } = JSON.parse(response.body);
      const definition: Definition = first(definitions);

      expect(definition).toEqual({
        id: "game/game",
        name: {
          "en-US": "game",
        },
        history: [{ hash: "somehash", more: "infos" }] as any,
        versions: {
          somehash: {
            name: {
              "en-US": "game-somehash",
            },
            description: {
              "en-US": "description-somehash",
            },
          },
        } as any,
      });
      done();
    });
  });
  /*
        xit('should deliver arrays of definitions on definition directory', async () => {
            const {definitions} = await fileAccessor.read('./game/game');
            // eslint-disable-next-line @typescript-eslint/no-var-requires
            expect(definitions).toEqual([require('./fixture/definitions/game/game.json')]);
        });

        xit('should deliver array of subfolders on higher level', async () => {
            try {
                expect(await fileAccessor.read('./game/generics')).toThrowError(Error);
            } catch (error) {
                expect(error.message).toEqual('Is Directory');
            }
        });

        xit('should fail on invalid path', async () => {
            try {
                expect(await fileAccessor.read('../inaccessiblepath/shouldneverbeopened.json')).toThrowError(NotFoundException);
            } catch (error) {
                expect(error.message).toEqual('No valid file found');
            }
        });

        xit('should respond accordingly on internal server error', async () => {
            try {
                expect(await fileAccessor.read('../inaccessiblepath/shouldneverbeopened.json')).toThrowError(NotFoundException);
            } catch (error) {
                expect(error.message).toEqual('No valid file found');
            }
        });*/
});
