import { Db, MongoClient } from "mongodb";
import { log } from "../logger/logger";

// Supposed to manage and pool mongodeb connections
export class MongoConnector {
  // TODO: Replace with pooled connection to allow for automatic reconnects
  private _client: MongoClient;

  get client(): MongoClient {
    // TODO: Return pooled connection
    return this._client;
  }

  // Async Constructor
  static async build(url?: string): Promise<MongoConnector> {
    log.trace({ url }, this.constructor.name);
    return new MongoConnector().connect(url);
  }

  db(name: string): Db {
    return this.client.db(name);
  }

  close(): Promise<void> {
    // TODO: Close all pooled connections
    return this.client.close();
  }

  private async connect(
    url = process.env.MONGO_SERVER || "mongodb://mongodb:27017",
  ): Promise<MongoConnector> {
    this._client = new MongoClient(url);
    await this._client.connect();
    return this;
  }
}

// Pseudo-Singleton that is supposed to manage and pool connections
// for general usage outside of Tests or CLI-scripts
// export const mongo = MongoConnector.build();
