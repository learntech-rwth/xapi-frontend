import * as fs from "fs-extra";

import {
  dbName,
  definitionCollection,
  definitionRenameCollection,
  definitionStructureCollection,
} from "./MongoConstants";
import { MongoConnector } from "./MongoConnector";
import { log } from "../logger/logger";
import { Index } from "../utils/Util";
import simpleGit, { SimpleGit } from "simple-git";
import { isNil } from "lodash";
import express from "express";

export async function isDirectory(path1: string): Promise<boolean> {
  return fs.existsSync(path1) && (await fs.lstat(path1)).isDirectory();
}

export function stripPrefix(path: string, prefix = "/"): string {
  const prefixPosition = path.indexOf(prefix);
  if (prefixPosition >= 0) {
    return path.substring(prefixPosition + prefix.length);
  }
  const fallBackPrefix = prefix.substring(0, prefix.length - 1);
  const fallBackPrefixPosition = path.indexOf(fallBackPrefix);
  if (fallBackPrefixPosition >= 0) {
    return path.substring(fallBackPrefixPosition + fallBackPrefix.length);
  }
  return path;
}

export async function repo2mongo(
  defDir: string,
  mongoserver = process.env.MONGO_SERVER,
  registryURL = process.env.DEFINITIONS_URL,
  prefix = process.env.STRIP_PREFIX,
  delay = 0,
  database = dbName,
  remoterepo?: string,
  rebuild = false,
): Promise<void> {
  log.info({ defDir, mongoserver }, "repo2mongo");
  log.debug(
    {
      defDir,
      mongoserver,
      database,
      registryURL,
      prefix,
      delay,
      remoterepo,
      rebuild,
    },
    "repo2mongo",
  );
  const repoURL = remoterepo;

  const client = await MongoConnector.build(mongoserver);
  log.info("Successfully connected to server");
  const db = client.db(database);
  const tempDefinitionCollection = "tDColl";
  const tempDefinitionStructureCollection = "tDSColl";
  const tempDefinitionRenames = "tDRename";
  try {
    await db.collection(tempDefinitionCollection).drop();
    await db.collection(tempDefinitionStructureCollection).drop();
    await db.collection(tempDefinitionRenames).drop();
  } catch (error) {
    log.info({ error }, "Nothing to drop");
  }
  await db.createCollection(tempDefinitionCollection);
  await db.createCollection(tempDefinitionStructureCollection);
  await db.createCollection(tempDefinitionRenames);

  log.info("Temporary Collections Created");
  let git: SimpleGit = null;

  if (!isNil(repoURL)) {
    if (rebuild) {
      git = simpleGit();
      await fs.emptyDir("/definitionserver/remoterepo");
      const res = await git.clone(repoURL, "/definitionserver/remoterepo");
      log.info({ res }, "git recloning complete");
    } else {
      git = simpleGit("/definitionserver/remoterepo");
      const res = await git.pull();
      log.info({ res }, "git pull result");
    }
    defDir = "/definitionserver/remoterepo/definitions";
    await git.cwd(defDir);
  }

  const subfolders: [string] = [defDir];
  const folderStructure: Index<string[]> = {};
  let i = 0;

  while (i < subfolders.length) {
    const content = await fs.readdir(subfolders[i]);
    const currentSubs: string[] = [];

    for (const element of content) {
      const currentPath = subfolders[i] + "/" + element;
      if (await isDirectory(currentPath)) {
        subfolders.push(currentPath);
        currentSubs.push(element);
      } else {
        //console.log(currentPath, "not a directory")
        const file = await fs.readFile(currentPath, "utf8");
        try {
          // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
          const definition = JSON.parse(file);
          let gitPath = currentPath;
          if (git) {
            const gitlog = await git.log({ file: gitPath });
            const versions: Index<string> = {};
            const historyList = [];
            const gitRawOutput = await git.raw([
              "log",
              "--follow",
              "-p",
              "--",
              gitPath,
            ]);
            const rawCommits = gitRawOutput.split("commit ");
            for (const revision of gitlog.all) {
              const history = {
                hash: revision.hash,
                date: revision.date,
                message: revision.message,
                author: revision.author_name,
                email: revision.author_email,
                type: "",
              };
              const relGitPath = stripPrefix(
                gitPath,
                "/definitionserver/remoterepo/",
              );

              let snapshot = "";
              try {
                snapshot = await git.show([revision.hash + ":" + relGitPath]);
              } catch {
                log.info(
                  { relGitPath },
                  "File might have moved, old version not accessible",
                );
                snapshot =
                  '{"warning":"File might have moved, old version not accessible"}';
              }
              try {
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                versions[revision.hash] = JSON.parse(snapshot);
              } catch {
                log.info(
                  { snapshot },
                  "Encountered invalid JSON in old snapshot",
                );
                // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
                versions[revision.hash] = JSON.parse(
                  '{"warning":"Invalid JSON for this definition in this commit"}',
                );
              }

              try {
                gitPath = rawCommits
                  .find((commit) => commit.startsWith(revision.hash))
                  .split("\n")
                  .map((line) => line.trim())
                  .find((line) => line.startsWith("rename from "))
                  .substring(12);
                await db.collection(tempDefinitionRenames).insertOne({
                  renameFrom: gitPath,
                  renamedTo: relGitPath,
                  commit: revision.hash,
                  date: revision.date,
                });
                history.type = "File moved";
              } catch {
                history.type = "File changed";
                //console.log("No obvious rename")
              }
              historyList.push(history);
            }
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            definition.history = historyList;
            // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
            definition.versions = versions;
          }
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          definition.id = stripPrefix(currentPath.replace(".json", ""), prefix)
            .split(" ")
            .join("_");
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-plus-operands
          definition.iri = (registryURL + definition.id).split(" ").join("_");
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          definition.path = stripPrefix(subfolders[i], prefix);
          // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
          definition.filename = element.replace(".json", "");

          // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
          await db.collection(tempDefinitionCollection).insertOne(definition);
          console.log("Definition inserted into MongoDB: ", definition);
        } catch (error) {
          log.warn({ error }, "Error");
        }
      }
    }
    const folder = stripPrefix(subfolders[i], prefix);
    log.info({ folder, prefix }, "Stripped Prefix");
    folderStructure[folder] = currentSubs.map((sub) =>
      stripPrefix(sub, prefix),
    );

    i++;
  }
  await db
    .collection(tempDefinitionStructureCollection)
    .insertOne(folderStructure);
  log.info({ folderStructure }, "Folder structure");

  await db.renameCollection(
    tempDefinitionStructureCollection,
    definitionStructureCollection,
    { dropTarget: true },
  );
  await db.renameCollection(tempDefinitionCollection, definitionCollection, {
    dropTarget: true,
  });
  await db.renameCollection(tempDefinitionRenames, definitionRenameCollection, {
    dropTarget: true,
  });
  log.info("Rename of temporary collections complete");
  await client.close();
}

export async function buildDatabaseWrapper(
  defDir: string,
  mongoserver = process.env.MONGO_SERVER,
  registryURL = process.env.DEFINITIONS_URL,
  prefix = process.env.STRIP_PREFIX,
  delay = 0,
  database = dbName,
  remoterepo?: string,
  rebuild = false,
  service = false,
): Promise<void> {
  if (service) {
    log.debug({ service }, "Starting RebuildService");
    const app = express();
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    app.all("/rebuild/", async (req, res) => {
      try {
        res.sendStatus(200);
        await repo2mongo(
          defDir,
          mongoserver,
          registryURL,
          prefix,
          delay,
          database,
          remoterepo,
          rebuild,
        );
        // res.send("Rebuild complete");
      } catch (error) {
        log.error({ error }, "RebuildDBError");
        res.sendStatus(500);
      }
    });
    //console.log("Get route set")
    const port = process.env.DBBUILDER_PORT || 5000;
    app.listen(port, () => {
      log.info(`Listening for rebuild requests on Port ${port}.`);
    });
  } else {
    log.debug({ service }, "Executing Rebuild");
    await repo2mongo(
      defDir,
      mongoserver,
      registryURL,
      prefix,
      delay,
      database,
      remoterepo,
      rebuild,
    );
  }
}

export const buildDataBase = buildDatabaseWrapper;

export const buildTestDataBase = async (
  fixture: string,
  registryURL = "https://xapistinkt.com/",
  prefix = "definitions/",
  testdb = "test",
  mongourl = process.env.MONGO_SERVER || "mongodb://mongodb:27017",
): Promise<MongoConnector> => {
  console.log("MongoURL", mongourl);
  console.log("Process.env", process.env.MONGO_SERVER);
  await buildDataBase(fixture, mongourl, registryURL, prefix, 0, testdb);
  return MongoConnector.build(mongourl);
};
