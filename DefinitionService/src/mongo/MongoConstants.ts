export const dbName = "definitions";
export const definitionStructureCollection = "DefStructure";
export const definitionCollection = "DefColl";
export const definitionRenameCollection = "DefRenames";
