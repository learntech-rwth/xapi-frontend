import pino from "pino";
import { isNil, isString } from "lodash";
import PinoHttp from "pino-http";

const options = {
  // @todo: is deprecated. Repair it
  //prettyPrint: !isNil(process.env.PINO_PRETTY) ? (process.env.PINO_PRETTY === 'true') : false
};
export const log = pino();
export const expressLog = PinoHttp();

const logLevels = ["trace", "debug", "info", "warn", "error", "fatal"];

function isLogLevel(level: unknown): boolean {
  return isString(level) && logLevels.includes(level);
}

const logLevel = isLogLevel(process.env.LOG_LEVEL)
  ? process.env.LOG_LEVEL
  : "debug";
log.level = logLevel;
log.info({ options, env: process.env.PINO_PRETTY }, "LogLevelCheck");
log.trace("trace");
log.debug("debug");
log.info("info");
log.warn("warn");
log.error("error");
