import { Index } from "../utils/Util";
import { isObject, isString } from "lodash";

// One definition has many DefinitionParts
// A DefinitionComponent renders one or more Definitions
// A DefinitionElement renders one DefinitionPart

export type DefinitionPart = LanguageMap | string;
export type DefinitionHistoryEntry = Index<string>;
export type DefinitionVersion = Omit<
  Definition,
  keyof DefinitionMetaInformation
>;
export type DefinitionMetaInformation = {
  versions?: Index<DefinitionVersion>;
  history?: DefinitionHistoryEntry[];
  id?: string;
  iri?: string;
  path?: string;
  filename?: string;
};

// TODO: Make id, iri and name mandatory
export type Definition = Index<DefinitionPart> & DefinitionMetaInformation;
export type LanguageMap<T = string> = Index<T>;

// validates if a given object is a valid languageMap
// https://www.typescriptlang.org/docs/handbook/advanced-types.html#type-guards-and-differentiating-types
export function isDefinitionLanguageMap(
  object: unknown,
): object is LanguageMap {
  if (!isObject(object)) {
    return false;
  }
  for (const key in object) {
    if (!isString((object as Index<unknown>)[key])) {
      return false;
    }
  }
  return true;
}

export function isDefinitionPart(object: unknown): object is DefinitionPart {
  return isString(object) || isDefinitionLanguageMap(object);
}

export function isDefinition(object: unknown): object is Definition {
  if (!isObject(object)) {
    return false;
  }
  for (const key in object) {
    if (!isDefinitionPart((object as Index<unknown>)[key])) {
      return false;
    }
  }
  return true;
}
