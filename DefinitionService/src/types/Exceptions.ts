export class Exception extends Error {
  constructor(
    message: string,
    readonly details?: object,
  ) {
    super(message);
  }
}

export class RequestException extends Exception {
  constructor(
    message: string,
    readonly status: number,
    details?: object,
  ) {
    super(message, details);
  }
}
