import { RequestException } from "../types/Exceptions";
import { Request } from "express";
import { NOT_FOUND } from "http-status-codes";
import { Definition } from "../types/DefinitionTypes";

export interface DefinitionAccessor<Type, TypeOptions> {
  read(
    req: Request,
    target: Type,
    options?: TypeOptions,
  ): Promise<DefinitionCrudResult>;
}

export type DefinitionCrudResult = {
  definitions?: Definition[];
  subfolders?: string[];
};

export type FilePath = string;

export class NotFoundException extends RequestException {
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  constructor(message: string = "Not Found", details?: object) {
    super(message, NOT_FOUND, details);
  }
}
