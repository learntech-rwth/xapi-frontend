import { isEmpty } from "lodash";
import {
  DefinitionAccessor,
  DefinitionCrudResult,
  FilePath,
  NotFoundException,
} from "./AccessorTypes";
import { Request } from "express";
import { MongoOptions } from "./MongoAccessor";

export class DefinitionCompositeAccessor
  implements DefinitionAccessor<FilePath, MongoOptions>
{
  constructor(readonly accessors: DefinitionAccessor<FilePath, {}>[]) {
    if (isEmpty(this.accessors)) {
      throw new Error("Empty Composite Accessor");
    }
  }

  async read(
    req: Request,
    target: FilePath,
    options?: MongoOptions,
  ): Promise<DefinitionCrudResult> {
    for (const accessor of this.accessors) {
      try {
        return await accessor.read(req, target, options);
      } catch (error) {
        if (!(error instanceof NotFoundException)) {
          throw error;
        }
      }
    }
    throw new NotFoundException("No Definitions Found");
  }
}
