import {
  DefinitionAccessor,
  DefinitionCrudResult,
  FilePath,
} from "./AccessorTypes";
import * as fs from "fs-extra";
import path from "path";
import { Request } from "express";

export abstract class AbstractPathAccessor
  implements DefinitionAccessor<FilePath, {}>
{
  constructor(readonly root: string) {}

  abstract doRead(
    req: Request,
    target: FilePath,
    options?: {},
  ): Promise<DefinitionCrudResult>;

  protected async isDirectory(path1: string): Promise<boolean> {
    return fs.existsSync(path1) && (await fs.lstat(path1)).isDirectory();
  }

  async read(
    req: Request,
    target: FilePath,
    options?: {},
  ): Promise<DefinitionCrudResult> {
    return this.doRead(req, await this.ensurePath(target), options);
  }

  // eslint-disable-next-line @typescript-eslint/require-await
  protected async ensurePath(target: FilePath): Promise<FilePath> {
    let result = target.replace("..", "");
    if (!result.startsWith(this.root)) {
      result = path.join(this.root, result);
    }
    return result;
  }
}
