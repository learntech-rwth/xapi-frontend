import { DefinitionFileAccessor } from "./FileAccessor";
import * as fs from "fs-extra";
import {
  DefinitionAccessor,
  DefinitionCrudResult,
  FilePath,
  NotFoundException,
} from "./AccessorTypes";
import { AbstractPathAccessor } from "./AbstractPathAccessor";
import { Request } from "express";

export class DefinitionDirectoryAccessor
  extends AbstractPathAccessor
  implements DefinitionAccessor<FilePath, {}>
{
  constructor(
    root: string,
    private fileAccessor: DefinitionFileAccessor = new DefinitionFileAccessor(
      root,
    ),
  ) {
    super(root);
  }

  async doRead(
    req: Request,
    contentFilePath: FilePath,
  ): Promise<DefinitionCrudResult> {
    const contentPath = contentFilePath;
    const content = await fs.readdir(contentPath);
    const response: DefinitionCrudResult = {
      definitions: [],
      subfolders: [],
    };

    for (const element of content) {
      if (await this.isDirectory(contentPath + "/" + element)) {
        response.subfolders.push(element);
      } else {
        const { definitions } = await this.tryReadFile(
          req,
          contentFilePath,
          element,
        );
        response.definitions.push(...definitions);
      }
    }

    return response;
  }

  protected async ensurePath(target: FilePath): Promise<FilePath> {
    const result = await super.ensurePath(target);
    if (fs.existsSync(result)) {
      return result;
    } else {
      throw new NotFoundException("Path not found");
    }
  }

  private async tryReadFile(
    req: Request,
    target: FilePath,
    element: string,
  ): Promise<DefinitionCrudResult> {
    try {
      return await this.fileAccessor.read(req, target + "/" + element);
    } catch (error) {
      if (!(error instanceof NotFoundException)) throw error;
      return { definitions: [] };
    }
  }
}
