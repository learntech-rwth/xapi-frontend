import { FilePath } from "../AccessorTypes";
import { Request } from "express";
import { removeTrailingSlash } from "../../utils/Util";

export function fakePathRequest(
  path: string,
  host = "host.name/",
  protocol = "https",
): Request {
  // noinspection JSUnusedLocalSymbols
  return {
    originalUrl: removeTrailingSlash(
      path.replace(/\.\/|\/\w*(\.json)*$/gm, ""),
    ),
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    get: (...values: never[]) => host,
    protocol,
  } as Request;
}

export function fakeFilePath(path: string): FilePath {
  return path;
}
