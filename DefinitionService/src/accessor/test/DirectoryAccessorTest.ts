import { DefinitionDirectoryAccessor } from "../DirectoryAccessor";
import path from "path";
import { NotFoundException } from "../AccessorTypes";
import { fakeFilePath, fakePathRequest } from "./TestHelpers";
import { Definition } from "../../types/DefinitionTypes";

const referenceArray: Definition[] = [
  {
    name: {
      "en-US": "doubleclicked",
    },
    description: {
      "en-US": "An actor clicked two times.",
    },
    iri: "https://host.name/game/doubleclicked",
  },
  {
    name: {
      "en-US": "game",
      "de-DE": "Spiel",
    },
    description: {
      "en-US": "Represents a game or competition of any kind.",
      "de-DE": "Repräsentiert jede Art von Spiel.",
    },
    iri: "https://host.name/game/game",
  },
];

xdescribe("[DefinitionDirectoryAccessor]", () => {
  const directoryAccessor = new DefinitionDirectoryAccessor(
    path.join(__dirname, "./fixture/definitions"),
  );

  it("should return an object with an array of all definition objects", async () => {
    const result = await directoryAccessor.read(
      fakePathRequest("./game"),
      fakeFilePath("./game"),
    );
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    expect(result.definitions).toEqual(referenceArray);
  });

  it("should return an object with an array of all subfolders", async () => {
    const result = await directoryAccessor.read(
      fakePathRequest("./game"),
      fakeFilePath("./game"),
    );
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    expect(result.subfolders).toEqual(["generics", "specifics"]);
  });

  it("should not allow access beyond root folder", async () => {
    try {
      expect(
        await directoryAccessor.read(
          fakePathRequest("../inaccessiblepath"),
          fakeFilePath("../inaccessiblepath"),
        ),
      ).toThrowError(NotFoundException);
    } catch (error: any) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      expect(error.message).toEqual("Path not found");
    }
  });
});
