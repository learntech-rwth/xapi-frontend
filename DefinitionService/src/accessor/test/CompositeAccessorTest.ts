import { DefinitionCompositeAccessor } from "../CompositeAccessor";
import {
  DefinitionAccessor,
  FilePath,
  NotFoundException,
} from "../AccessorTypes";
import { anything, instance, mock, resetCalls, verify, when } from "ts-mockito";
import { fakeFilePath, fakePathRequest } from "./TestHelpers";

xdescribe("[CompositeAccessor]", () => {
  const FirstAccessorMock = mock<DefinitionAccessor<FilePath, {}>>();
  const firstAccessor = instance(FirstAccessorMock);

  const pathToDefinition = fakeFilePath("path/to/definition");
  const pathTo = fakeFilePath("path/to");
  const pathToServerError = fakeFilePath("path/to/servererror");

  const pathToDefinitionRequest = fakePathRequest("path/to/definition");
  const pathToRequest = fakePathRequest("path/to");
  const pathToServerErrorRequest = fakePathRequest("path/to/servererror");

  when(
    FirstAccessorMock.read(pathToDefinitionRequest, pathToDefinition),
  ).thenResolve({ definitions: [{ name: "definition" }] });
  when(FirstAccessorMock.read(pathToRequest, pathTo)).thenReject(
    new NotFoundException("Not Found"),
  );
  when(
    FirstAccessorMock.read(pathToServerErrorRequest, pathToServerError),
  ).thenReject(new Error("Critical"));

  const SecondAccessorMock = mock<DefinitionAccessor<FilePath, {}>>();
  const secondAccessor = instance(SecondAccessorMock);

  when(SecondAccessorMock.read(pathToRequest, pathTo)).thenResolve({
    definitions: [{ name: "definition" }, { name: "anotherdefinition" }],
  });

  let compositeAccessor: DefinitionCompositeAccessor;

  beforeAll(() => {
    compositeAccessor = new DefinitionCompositeAccessor([
      firstAccessor,
      secondAccessor,
    ]);
  });

  beforeEach(() => {
    resetCalls(FirstAccessorMock);
    resetCalls(SecondAccessorMock);
  });

  it("should call first accessor first and second accessor never", async () => {
    const result = await compositeAccessor.read(
      pathToDefinitionRequest,
      pathToDefinition,
    );
    console.log("result", result);
    expect(result).toEqual({ definitions: [{ name: "definition" }] });
    verify(FirstAccessorMock.read(anything(), anything())).once();
    verify(SecondAccessorMock.read(anything(), anything())).never();
  });

  it("should call first accessor first, then second accessor after failing", async () => {
    const result = await compositeAccessor.read(pathToRequest, pathTo);
    expect(result).toEqual({
      definitions: [{ name: "definition" }, { name: "anotherdefinition" }],
    });
    verify(FirstAccessorMock.read(anything(), anything())).once();
    verify(SecondAccessorMock.read(anything(), anything())).once();
  });

  it("should not catch internal server error and interrupt call chain", async () => {
    try {
      await compositeAccessor.read(pathToServerErrorRequest, pathToServerError);
    } catch (error) {
      expect(error).toEqual(new Error("Critical"));
    }
    verify(FirstAccessorMock.read(anything(), anything())).once();
    verify(SecondAccessorMock.read(anything(), anything())).never();
  });
});
