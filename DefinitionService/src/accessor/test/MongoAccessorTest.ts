import { buildTestDataBase } from "../../mongo/buildDatabase";
import { MongoAccessor } from "../MongoAccessor";
import {
  definitionCollection,
  definitionStructureCollection,
} from "../../mongo/MongoConstants";
import { MongoConnector } from "../../mongo/MongoConnector";

describe("[MongoAccessor]", () => {
  let accessor: MongoAccessor;
  let client: MongoConnector;

  beforeAll(async () => {
    client = await buildTestDataBase("src/accessor/test/fixture/definitions");
    const db = client.db("test");
    const collection = db.collection(definitionCollection);
    const structure = db.collection(definitionStructureCollection);
    accessor = new MongoAccessor(collection, structure);
  });

  afterAll(() => {
    return client.close();
  });

  describe("[single file]", () => {
    it('when the filepath "accessor/test/fixture/definitions/game/generics/game" is requested, then [MongoAccessor] returns game.json and meta-information is filtered', async () => {
      const result = await accessor.read(null, "game/generics/game");
      expect(result).toEqual({
        definitions: [
          {
            id: "game/generics/game",
            name: { "en-US": "generic game" },
            description: {
              "en-US": "Represents a generic game or competition of any kind.",
            },
            iri: "https://xapistinkt.com/game/generics/game",
          },
        ],
      });
    });

    it("when the filepath 'accessor/test/fixture/definitions/game/generics/doesnotexist' is requested, then [MongoAccessor] returns nothing", async () => {
      const result = await accessor.read(
        null,
        "accessor/test/fixture/definitions/game/generics/doesnotexist",
      );
      expect(result).toEqual({});
    });

    // xit('when the filepath definitions/game/game.json is requested with the additional field [path], then [MongoAccessor] return game.json and it includes the [path]-field', () => {
    //
    // });
  });

  describe("[directory with multiple files and/or subfolders]", () => {
    it("when the filepath definitions/game/generics is requested, then [MongoAccessor] returns game.json and doubleclicked.json", async () => {
      const result = await accessor.read(null, "game/generics");
      expect(result).toEqual({
        subfolders: [],
        definitions: [
          {
            id: "game/generics/doubleclicked",
            name: { "en-US": "generic doubleclicked" },
            description: { "en-US": "An actor generically clicked two times." },
            iri: "https://xapistinkt.com/game/generics/doubleclicked",
          },
          {
            id: "game/generics/game",
            name: { "en-US": "generic game" },
            description: {
              "en-US": "Represents a generic game or competition of any kind.",
            },
            iri: "https://xapistinkt.com/game/generics/game",
          },
        ],
      });
    });

    it("when the filepath definitions/game is requested, then [MongoAccessor] returns game.json and doubleclicked.json as well as the subfolders", async () => {
      const result = await accessor.read(null, "game");
      expect(result).toEqual({
        subfolders: ["generics", "specifics"],
        definitions: [
          {
            id: "game/doubleclicked",
            name: { "en-US": "doubleclicked" },
            description: { "en-US": "An actor clicked two times." },
            iri: "https://xapistinkt.com/game/doubleclicked",
          },
          {
            id: "game/game",
            name: { "en-US": "game" },
            description: {
              "en-US": "Represents a game or competition of any kind.",
            },
            iri: "https://xapistinkt.com/game/game",
          },
        ],
      });
    });

    it("when a filepath of a higher level folder without definitions is requested only the subfolders are returned", async () => {
      const result = await accessor.read(null, "");
      expect(result).toEqual({ subfolders: ["game"] });
    });
  });

  describe("RequestOptions", () => {
    it("when a definition is requested with specific fields, then all of those fields are returned", async () => {
      const result = await accessor.read(null, "game/game", {
        fields: ["id", "name", "path", "filename"],
      });
      expect(result).toEqual({
        definitions: [
          {
            id: "game/game",
            name: { "en-US": "game" },
            filename: "game",
            path: "game",
          },
        ],
      });
    });
  });
});
