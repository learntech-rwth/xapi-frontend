import { DefinitionFileAccessor } from "../FileAccessor";
import path from "path";
import { NotFoundException } from "../AccessorTypes";
import { fakeFilePath, fakePathRequest } from "./TestHelpers";
import { first } from "lodash";

xdescribe("[DefinitionFileAccessor]", () => {
  const fileAccessor = new DefinitionFileAccessor(
    path.join(__dirname, "./fixture/definitions"),
  );

  it("should open a file", async () => {
    const { definitions } = await fileAccessor.read(
      fakePathRequest("./game/game.json"),
      fakeFilePath("./game/game.json"),
    );
    // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-unsafe-assignment
    const file = require("./fixture/definitions/game/game.json");
    // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
    expect(first(definitions)).toEqual(jasmine.objectContaining(file));
  });

  it("should generate IRI and add to JSON", async () => {
    const { definitions } = await fileAccessor.read(
      fakePathRequest("./game/doubleclicked.json"),
      fakeFilePath("./game/doubleclicked.json"),
    );
    const definition = first(definitions);
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    expect(definition.iri).toEqual("https://host.name/game/doubleclicked");
  });

  it("should open a file if a json-file with the same name exists", async () => {
    const { definitions } = await fileAccessor.read(
      fakePathRequest("./game/game"),
      fakeFilePath("./game/game"),
    );
    // eslint-disable-next-line @typescript-eslint/no-var-requires,@typescript-eslint/no-unsafe-argument
    expect(first(definitions)).toEqual(
      jasmine.objectContaining(require("./fixture/definitions/game/game.json")),
    );
  });

  it("should generate IRI and add to JSON", async () => {
    const { definitions } = await fileAccessor.read(
      fakePathRequest("./game/doubleclicked"),
      fakeFilePath("./game/doubleclicked"),
    );
    const definition = first(definitions);
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    expect(definition.iri).toEqual("https://host.name/game/doubleclicked");
  });

  it("should throw is dir when param is existing directory", async () => {
    try {
      expect(
        await fileAccessor.read(
          fakePathRequest("./game/generics"),
          fakeFilePath("./game/generics"),
        ),
      ).toThrowError(Error);
    } catch (error: any) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      expect(error.message).toEqual("Is Directory");
    }
  });

  it("should not allow access beyond root folder", async () => {
    try {
      expect(
        await fileAccessor.read(
          fakePathRequest("../inaccessiblepath/shouldneverbeopened.json"),
          fakeFilePath("../inaccessiblepath/shouldneverbeopened.json"),
        ),
      ).toThrowError(NotFoundException);
    } catch (error: any) {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      expect(error.message).toEqual("No valid file found");
    }
  });
});
