import {
  DefinitionAccessor,
  DefinitionCrudResult,
  FilePath,
  NotFoundException,
} from "./AccessorTypes";
import * as fs from "fs-extra";
import { AbstractPathAccessor } from "./AbstractPathAccessor";
import { log } from "../logger/logger";
import * as _ from "lodash";
import { Request } from "express";
import { isDefinition } from "../types/DefinitionTypes";

export function buildIRI(req: Request, contentPath: string): string {
  return (
    req.protocol +
    "://" +
    req.get("Host") +
    req.originalUrl +
    "/" +
    _.last(contentPath.split("/")).replace(".json", "")
  );
}

export class DefinitionFileAccessor
  extends AbstractPathAccessor
  implements DefinitionAccessor<FilePath, {}>
{
  constructor(root: string) {
    super(root);
    log.debug({ root: root });
  }

  async doRead(
    req: Request,
    filePath: FilePath,
  ): Promise<DefinitionCrudResult> {
    const file = await fs.readFile(filePath, "utf8");
    const definition = JSON.parse(file);

    definition.iri = buildIRI(req, filePath);

    log.debug({ filePath: filePath }, "DoReadFilePath");

    if (isDefinition(definition)) {
      return {
        definitions: [definition],
      };
    } else throw new Error("Invalid definition");
  }

  protected async ensurePath(target: FilePath): Promise<FilePath> {
    let filePath = await super.ensurePath(target);
    if (await this.isDirectory(filePath)) {
      throw new NotFoundException("Is Directory");
    }
    filePath = this.ensureEnding(filePath);

    if (!fs.existsSync(filePath)) {
      throw new NotFoundException("No valid file found");
    }
    return filePath;
  }

  // noinspection JSMethodCanBeStatic
  private ensureEnding(target: string): string {
    if (target.endsWith(".json")) {
      return target;
    }
    return `${target}.json`;
  }
}
