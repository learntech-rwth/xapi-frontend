import {
  DefinitionAccessor,
  DefinitionCrudResult,
  FilePath,
} from "./AccessorTypes";
import { Request } from "express";
import { Collection } from "mongodb";
import { isEmpty } from "lodash";
import { log } from "../logger/logger";
import { Definition } from "../types/DefinitionTypes";
import { Index } from "../utils/Util";

export type MongoOptions = { fields?: string[] };

export class MongoAccessor
  implements DefinitionAccessor<FilePath, MongoOptions>
{
  readonly projectableFields = [
    "id",
    "name",
    "path",
    "iri",
    "description",
    "filename",
    "versions",
    "history",
  ];
  readonly privateFields = ["_id"];

  constructor(
    private definitions: Collection,
    private structure: Collection,
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  async read(
    req: Request,
    target: FilePath,
    options?: MongoOptions,
  ): Promise<DefinitionCrudResult> {
    log.trace({ target }, this.constructor.name);

    const result: DefinitionCrudResult = {};
    let definitions: Definition[];

    const projection: Index<number> = this.customProjection(options?.fields);

    const folderTree = await this.structure.findOne<Index<string[]>>({});
    // eslint-disable-next-line no-prototype-builtins
    if (folderTree.hasOwnProperty(target)) {
      result.subfolders = folderTree[target];
      definitions = await this.definitions
        .find<Definition>({ path: target }, { projection: projection })
        .toArray();
    } else
      definitions = await this.definitions
        .find<Definition>({ id: target }, { projection: projection })
        .toArray();

    if (!isEmpty(definitions)) {
      result.definitions = definitions;
    }
    log.trace({ result }, this.constructor.name);
    return result;
  }

  private customProjection(fields?: string[]): Index<number> {
    const customProjection: Index<number> = {};
    if (!isEmpty(fields)) {
      fields
        .filter((field) => this.isProjectableField(field))
        .forEach((field) => {
          customProjection[field] = 1;
        });
      this.privateFields.forEach((field) => {
        customProjection[field] = 0;
      });
      return customProjection;
    } else {
      return {
        id: 1,
        name: 1,
        iri: 1,
        description: 1,
        _id: 0,
      };
    }
  }

  private isProjectableField(field: string): boolean {
    return (
      !this.privateFields.includes(field) &&
      this.projectableFields.includes(field)
    );
  }
}
