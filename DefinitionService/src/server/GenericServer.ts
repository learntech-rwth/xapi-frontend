import { log } from "../logger/logger";
import express, { Express } from "express";
import { Server } from "http";
import { MongoConnector } from "../mongo/MongoConnector";
import bodyParser from "body-parser";

export type MiddlewareHandler = (app: Express) => void;
export type ServerConfig = {
  middlewareHandler: MiddlewareHandler;
  port?: number;
  mongo?: MongoConnector | Promise<MongoConnector>;
};

export class GenericServer {
  readonly server: Server;
  readonly app: Express;
  private mongo: MongoConnector;

  private resolveStarted: (value?: boolean | PromiseLike<boolean>) => void;
  private rejectStarted: (value?: unknown) => void;
  readonly started = new Promise<boolean>((resolve, reject) => {
    this.resolveStarted = resolve;
    this.rejectStarted = reject;
  });

  constructor(config: ServerConfig) {
    try {
      this.app = express();
      this.app.use(bodyParser.urlencoded({ extended: false }));
      this.app.use(bodyParser.json());

      config.middlewareHandler(this.app);

      const port = config.port || process.env.DEFINITION_PORT || 0;
      this.server = this.app.listen(port, () => {
        Promise.resolve()
          .then(() => {
            return config.mongo;
          })
          .then((result) => {
            this.mongo = result;
            this.resolveStarted(true);
            log.info(`Starting ExpressJS server on Port ${port}.`);
          });
      });
    } catch (error) {
      this.rejectStarted(error);
      log.fatal(error, "Server Initialization Failed");
    }
  }

  close(): void {
    this.mongo.close().catch((err) => {
      log.error(err, "Closing MongoDB Connection Failed");
    });
    this.server.close();
  }
}
