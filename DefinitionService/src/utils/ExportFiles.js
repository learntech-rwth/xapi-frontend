import JsPDF from "jspdf";
import "jspdf-autotable";

// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
export function exportPDF(items, lang) {
  const definitionsArray = items.map((element) => {
    return [element.name, element.path, element.description, element.iri];
  });

  const doc = new JsPDF({
    orientation: "landscape",
    format: "a4",
    unit: "mm",
  });

  const header = {
    en: [["Name", "Path", "Description", "IRI"]],
  }[lang] || [["Name", "Pfad", "Beschreibung", "IRI"]];

  doc.autoTable({
    head: header,
    body: definitionsArray,
    styles: {
      fontSize: 9,
      font: "helvetica",
      overflow: "linebreak",
      //overflow: "ellipsize",
    },
    columnStyles: {
      0: { cellWidth: 30 },
      1: { cellWidth: 30 },
      2: { cellWidth: 110 },
      3: { cellWidth: 100 },
    },
  });

  doc.setFont("helvetica");
  doc.setFontSize(10);
  doc.text(
    15,
    doc.lastAutoTable.finalY + 10,
    `Abgerufen am ${new Date().toLocaleDateString()} um ${new Date().toLocaleTimeString()}`,
  );
  doc.text(
    15,
    doc.lastAutoTable.finalY + 14,
    `https://xapi.elearn.rwth-aachen.de`,
  );
  doc.save(`xAPI - ${new Date().toLocaleDateString()}.pdf`);
}

export function exportCSV(items, lang) {
  let header;
  if (lang === "en") {
    header = ["Name", "Path", "Description", "IRI"];
  } else {
    header = ["Name", "Pfad", "Beschreibung", "IRI"];
  }

  let rows = items.map((element) => {
    return [element.name, element.path, element.description, element.iri];
  });
  rows.unshift(header);

  let csvContent;
  rows.forEach(function (rowArray) {
    let row = rowArray.join(",");
    csvContent += row + "\r\n";
  });

  const element = document.createElement("a");
  element.setAttribute(
    "href",
    `data:text/csv;charset=utf-8,%EF%BB%BF${encodeURI(
      csvContent.substring(9),
    )}`,
  );
  element.setAttribute(
    "download",
    `xAPI - ${new Date().toLocaleDateString()}.csv`,
  );
  element.click();
}
