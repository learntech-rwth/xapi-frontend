import { Request, Response } from "express";
import { BAD_REQUEST } from "http-status-codes";
import { log } from "../logger/logger";

export function ErrorMiddleWare(
  error: Error,
  req: Request,
  res: Response,
): void {
  if (!res.headersSent) {
    res.status(BAD_REQUEST).send("Bad Request");
  }
  log.error(error, "ErrorMiddleWare");
}
