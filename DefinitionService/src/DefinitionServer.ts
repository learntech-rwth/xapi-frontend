import dotenv from "dotenv";
dotenv.config();

import compression from "compression";
import helmet from "helmet";
import express, { Express, NextFunction, Request, Response } from "express";
import path from "path";
import { DefinitionRouter } from "./router/DefinitionRouter";
import { ErrorMiddleWare } from "./middleware/ErrorMiddleWare";
import { expressLog, log } from "./logger/logger";
import { MongoAccessor } from "./accessor/MongoAccessor";
import {
  dbName,
  definitionCollection,
  definitionStructureCollection,
} from "./mongo/MongoConstants";
import { Server } from "http";
import bodyParser from "body-parser";
import { MongoConnector } from "./mongo/MongoConnector";

if (process.env.NODE_ENV !== "production") {
  log.warn({ NODE_ENV: process.env.NODE_ENV }, "NoProductionEnvironment");
} else {
  log.warn({ NODE_ENV: process.env.NODE_ENV }, "StartedProductionEnvironment");
}
log.info(process.env.parsed, "Environment");

function interceptHtmlRequest(
  req: Request,
  res: Response,
  next: NextFunction,
): void {
  // respond with html page
  if (req.accepts("html") && !res.headersSent) {
    res.set("Content-Type", "text/html");
    // eslint-disable-next-line @typescript-eslint/no-use-before-define
    res.sendFile(path.join(DefinitionServer.publicFolder, "/index.html"));
    return;
  }
  next();
}

class DefinitionServer {
  private server: Server;
  private app: Express;

  static readonly rootFolder = path.join(process.cwd());
  static readonly publicFolder = path.join(
    DefinitionServer.rootFolder,
    "/public",
  );

  async start(): Promise<DefinitionServer> {
    this.app = express();
    this.applyMiddleWare();
    await this.applyRouters();
    this.app.use(ErrorMiddleWare);

    const port = process.env.DEFINITION_PORT;
    this.server = this.app.listen(port, () =>
      log.info(`Starting ExpressJS server on Port ${port}.`),
    );

    return this;
  }

  private async buildDefinitionRouter(): Promise<DefinitionRouter> {
    const db = (await MongoConnector.build()).db(dbName);
    const collection = db.collection(definitionCollection);
    const structure = db.collection(definitionStructureCollection);
    const mongoAccessor = new MongoAccessor(collection, structure);
    return new DefinitionRouter(mongoAccessor);
  }

  private async applyRouters(): Promise<void> {
    this.app.use(interceptHtmlRequest);
    const definitionRouter = await this.buildDefinitionRouter();
    this.app.use("/definitions", definitionRouter.router);
  }

  private applyMiddleWare(): void {
    this.app.use(expressLog);
    this.app.use(bodyParser.urlencoded({ extended: false }));
    this.app.use(bodyParser.json());
    this.app.use(helmet()); // set well-known security-related HTTP headers
    this.app.use(compression());
    this.app.disable("x-powered-by");

    log.warn({ "Static Folder": DefinitionServer.publicFolder });
    this.app.use(express.static(DefinitionServer.publicFolder));
  }
}

export const server = new DefinitionServer().start();
