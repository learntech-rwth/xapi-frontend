const { FUNCTIONS_API } = process.env;

// Check if relevant request header is set. If true, import and return definitions and subfolders in JSON format.
export default async function (context) {
  if (
    context.req &&
    context.req.headers &&
    ((context.req.headers["content-type"] &&
      context.req.headers["content-type"] === "application/json") ||
      (context.req.headers["accept"] &&
        context.req.headers["accept"] === "application/json"))
  ) {
    const response = await writeJSON(context.req.originalUrl, context);
    context.res.writeHead(200, { "Content-Type": "application/json" });
    context.res.end(JSON.stringify(response, null, 2));
  }
}

// Create JSON file by making calls to functionsAPI and return JSON object
async function writeJSON(url, context) {
  const caseOfExport = url.substring(0, url.substring(1).indexOf("/"));
  const defUrl = url.substring("/definitions".length);
  const basketUrl = url.substring("/baskets".length);
  // if definition is requested
  if (caseOfExport === "/definition") {
    // fetch definitions
    const defResponse = await context.app.$axios.get(
      `${FUNCTIONS_API}/definitions`,
      { params: { def: defUrl } },
    );
    const definitions = defResponse.data.map(
      ({ name, description, id, iri }) => {
        return { name, description, id, iri };
      },
    );

    // fetch subfolders
    const isRoot = defUrl === "/" || defUrl === "";
    const { data } = await context.app.$axios.get(
      `${FUNCTIONS_API}/definitions/folders`,
    );
    const key = isRoot ? "" : defUrl.substring(1);
    const subfolders = data[0][key];

    return { subfolders, definitions };
  }
  // if basket is requested
  else if (caseOfExport === "/basket") {
    const basketResponse = await context.app.$axios.get(
      `${FUNCTIONS_API}/baskets${basketUrl}`,
    );
    const definitions = basketResponse.data.map(
      ({ name, description, id, iri }) => {
        return { name, description, id, iri };
      },
    );

    return { definitions };
  }
  // if not specified
  else {
    const { data } = await context.app.$axios.get(
      `${FUNCTIONS_API}/definitions/folders`,
    );
    const subfolders = data[0][""];

    return { subfolders };
  }
}
