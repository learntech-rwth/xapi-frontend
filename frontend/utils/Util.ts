import { isArray, isEmpty, isString } from "lodash";

export interface Index<T> {
  [key: string]: T;
}

export type Items<T> = T | T[];

export function asArray<T>(item: Items<T>): T[] {
  if (isArray(item)) {
    return item as T[];
  } else {
    return [item] as T[];
  }
}

export function removeTrailingSlash(s: string): string {
  return s.replace(/\/$/, "");
}

const ignoredParts = ["id", "versions", "history"];
export const filterIgnoredDefinitonParts: (key: string) => boolean = (key) =>
  !ignoredParts.includes(key);

export function isStringArray(arr: any[]): arr is string[] {
  return isEmpty(arr.filter((val) => !isString(val)));
}
