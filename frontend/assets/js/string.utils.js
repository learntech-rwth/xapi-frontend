// Check if string is alphanumeric
export function isAlphaNumeric(str) {
  for (let i = 0, len = str.length; i < len; i++) {
    const code = str.charCodeAt(i);
    if (
      !(code > 47 && code < 58) &&
      !(code > 64 && code < 91) &&
      !(code > 96 && code < 123)
    ) {
      return false;
    }
  }
  return true;
}
