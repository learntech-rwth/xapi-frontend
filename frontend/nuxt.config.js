require("dotenv").config();

const { FUNC_API_URL, DEF_API_URL, DEF_SERVICE_URL } = process.env;

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "xAPI Definitions Registry",
    htmlAttrs: {
      lang: ["en", "de"],
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/png", href: "favicon.png" }],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/main.css"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: ["@nuxtjs/vuetify", "@nuxtjs/dotenv"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    "nuxt-i18n",
    "nuxt-ssr-cache",
    "@nuxtjs/toast",
    "@nuxtjs/proxy",
    "@nuxtjs/axios",
    "@nuxtjs/recaptcha",
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    standalone: true,
  },

  // Import translations: https://i18n.nuxtjs.org/
  i18n: {
    strategy: "no_prefix",
    lazy: false,
    defaultLocale: "en",
    langDir: "locales/",
    locales: [
      {
        name: "English",
        code: "en",
        iso: "en",
        file: "en",
      },
      {
        name: "Deutsch",
        code: "de",
        iso: "de",
        file: "de",
      },
    ],
  },

  // Extend routes: https://nuxtjs.org/docs/features/file-system-routing/#extending-the-router
  router: {
    middleware: ["content-type"],
    extendRoutes(routes, resolve) {
      routes.push(
        {
          path: "/definitions*",
          component: resolve(__dirname, "pages/definitions.vue"),
        },
        {
          path: "/baskets*",
          component: resolve(__dirname, "pages/baskets.vue"),
        },
      );
    },
  },

  // Enable SSR caching: https://www.npmjs.com/package/nuxt-ssr-cache
  cache: {
    useHostPrefix: false,
    pages: ["/"],
    store: {
      type: "memory",
      max: 20,
      ttl: 900,
    },
  },

  // Enabling Nuxt Toasts: https://www.npmjs.com/package/@nuxtjs/toast
  toast: {
    position: "top-right",
    duration: 4000,
    keepOnHover: false,
    iconPack: "mdi",
    closeOnSwipe: true,
  },

  // Enabling axios to use proxy
  axios: {
    proxy: true,
  },

  // Run Frontend in DOCKER
  // Enabling Nuxt Proxy for Production Mode: https://www.npmjs.com/package/@nuxtjs/proxy
  proxy: {
    "/f-api": FUNC_API_URL,
    "/d-api": DEF_API_URL,
    "/rebuild": DEF_SERVICE_URL,
  },

  // Enabling Google reCaptcha: https://www.npmjs.com/package/@nuxtjs/recaptcha
  recaptcha: {
    hideBadge: false,
    language: "en",
    siteKey: "6LfVzG4eAAAAAOBEyPu8l1E94RFumyExK6YnijeW",
    version: 3,
    size: "normal",
  },
};
