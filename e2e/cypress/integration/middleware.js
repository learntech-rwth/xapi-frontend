describe("Request frontend without triggering middleware", () => {
  it("Check reply", () => {
    cy.visit(process.env.REGISTRY_URL);
    cy.request(process.env.REGISTRY_URL).then((response) => {
      expect(response.status).to.equal(200);
      expect(response.headers["content-type"]).to.contain("text/html");
      expect(response.body).to.contain(
        "<title>xAPI Definitions Registry</title>",
      );
    });
  });
});

describe("Request frontend whilst triggering middleware", () => {
  it("Check reply", () => {
    cy.request({
      url: process.env.REGISTRY_URL,
      headers: {
        Accept: "application/json",
      },
    }).then((response) => {
      expect(response.status).to.equal(200);
      expect(response.headers["content-type"]).to.contain("application/json");
      expect(response.body.subfolders.length).to.be.greaterThan(10);
    });
    cy.request({
      url: `${process.env.REGISTRY_URL}/definitions/generic/verbs`,
      headers: {
        Accept: "application/json",
      },
    }).then((response) => {
      expect(response.status).to.equal(200);
      expect(response.headers["content-type"]).to.contain("application/json");
      expect(response.body.definitions.length).to.be.greaterThan(10);
    });
  });
});
