describe("Request frontend overview page and check for entries", () => {
  it("Check reply for entries", () => {
    cy.visit(process.env.REGISTRY_URL);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 400);
  });
});
