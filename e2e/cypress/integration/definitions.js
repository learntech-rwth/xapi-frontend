describe("Request frontend definitions page and check for entries", () => {
  it("Check reply for entries", () => {
    cy.visit(`${process.env.REGISTRY_URL}/definitions`);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 10);
  });
  it("Check reply for entries", () => {
    cy.visit(`${process.env.REGISTRY_URL}/definitions/generic`);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 2);
  });
  it("Check reply for entries", () => {
    cy.visit(`${process.env.REGISTRY_URL}/definitions/generic/verbs`);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 20);
  });
});
