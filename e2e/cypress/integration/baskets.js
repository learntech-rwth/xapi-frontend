describe("Create basket and check basket for entries", () => {
  it("Check created baskets for entries", () => {
    cy.visit(process.env.REGISTRY_URL);
    cy.wait(200);
    cy.get(".v-btn").eq(40).click();
    cy.get(".v-btn").eq(50).click();
    cy.get(".v-btn").eq(70).click();

    cy.visit(`${process.env.REGISTRY_URL}/cart`);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 2);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.lt", 7);
    cy.get(".ma-7").eq(1).click();
    cy.get(".ma-7").eq(0).click();

    cy.url().should("contain", "/baskets");
    cy.wait(1000);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.gt", 3);
    cy.get(".v-data-table")
      .find("tbody")
      .children()
      .should("have.length.lt", 8);
  });
});
