"use strict";

import initServer from "./initServer.js";

const app = initServer(process.env.FUNC_API_PORT || 1000);

import basketsRoute from "./routes/f-api/baskets.route.js";
import definitionsRoute from "./routes/f-api/definitions.route.js";

app.register(basketsRoute, { prefix: "/f-api/baskets" });
app.register(definitionsRoute, { prefix: "/f-api/definitions" });

async function main() {}

main().catch(console.error);
