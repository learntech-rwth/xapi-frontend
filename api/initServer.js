"use strict";

import dotenv from "dotenv";
dotenv.config();

import fastify from "fastify";

import cors from "@fastify/cors";
import * as boom from "fastify-boom";

export default function initServer(port) {
  const app = fastify({
    logger: true,
    ignoreTrailingSlash: true,
    bodyLimit: 1048576,
  });

  // Enable cors
  app.register(cors, {
    origin: "*",
  });

  // Enable boom error beautification
  app.register(boom);

  // Start API Server and import folder structure
  (async () => {
    try {
      await app.listen({
        port,
        host: "0.0.0.0",
      });
    } catch (err) {
      app.log.error(err);
      process.exit(1);
    }
  })();

  return app;
}
