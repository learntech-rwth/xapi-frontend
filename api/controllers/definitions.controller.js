// Return all folders which already were imported from mongodb
import db from "../db.js";

export async function fetchFolders(req, res) {
  const mongo = db();

  await mongo.connect();
  const folders = await mongo
    .db("definitions")
    .collection("DefStructure")
    .find({})
    .toArray();
  await mongo.close();

  res.code(200).send(folders);
}

function clearPath(path) {
  if (!path) return "";
  if (path[0] === "/") {
    if (path[path.length - 1] === "/")
      return path.substring(1, path.length - 2);

    return path.substring(1);
  }
  return path;
}

// Return all data of given definition
// Import single definition details to result array
export async function fetchOneDefinition(req, res) {
  const def = clearPath(req.query.def);

  const mongo = db();

  await mongo.connect();
  const result = await mongo
    .db("definitions")
    .collection("DefColl")
    .find({
      $or: [{ id: def }, { path: def }],
    })
    .toArray();
  await mongo.close();

  if (!result) {
    return;
  }

  res.code(200).send(result);
}

// Return all data of given definition
// Import single definition details to result array
export async function fetchDefinitions(req, res) {
  const defs = req.query["defs[]"];

  if (!req.query.def && !defs) {
    res.code(200).send([]);
    return;
  } else if (req.query.def) {
    await fetchOneDefinition(req, res);
    return;
  }

  const mongo = db();

  await mongo.connect();
  const result = await mongo
    .db("definitions")
    .collection("DefColl")
    .find({
      iri: { $in: defs },
    })
    .toArray();
  await mongo.close();

  res.code(200).send(result);
}
