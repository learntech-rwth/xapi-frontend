import db from "../db.js";

// Add given definition to given basket and import result to result array
export async function addBasket(req, res) {
  const mongo = db();
  const { defs, basket, def } = req.body;

  if (defs) {
    await mongo.connect();
    const result = await mongo
      .db("baskets")
      .collection(basket)
      .insertMany(
        defs.map((def) => {
          return { id: def };
        }),
      );
    await mongo.close();
    res.code(200).send(result);
    return;
  }

  await mongo.connect();
  const result = await mongo.db("baskets").collection(basket).insertOne({
    id: def,
  });
  await mongo.close();

  res.code(200).send(result);
}

// Remove given definition from given basket and import result to result array
export async function removeBasket(req, res) {
  const basket = req.params.id;
  const { defs, def } = req.body;

  const mongo = db();

  if (defs) {
    await mongo.connect();
    const result = await mongo
      .db("baskets")
      .collection(basket)
      .deleteMany({
        id: { $in: defs },
      });
    await mongo.close();
    res.code(200).send(result);
    return;
  }

  await mongo.connect();
  const result = await mongo.db("baskets").collection(basket).deleteOne({
    id: def,
  });
  await mongo.close();

  res.code(200).send(result);
}

// Import all definitions from given basket to result array
export async function getBasket(req, res) {
  const basketId = req.params.id;
  const mongo = db();

  await mongo.connect();

  const defs = await mongo
    .db("baskets")
    .collection(basketId)
    .find({})
    .toArray();
  const results = [];
  for (let i = 0; i < defs.length; i++) {
    if (!defs[i]) continue;
    const res = await mongo
      .db("definitions")
      .collection("DefColl")
      .find({
        iri: defs[i].id,
      })
      .toArray();
    results.push(res);
  }
  await mongo.close();
  const baskets = results.flat(1);

  res.code(200).send(baskets);
}
