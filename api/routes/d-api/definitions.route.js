import db from "../../db.js";

export default function (fastify, opts, done) {
  // Return all definitions which already were imported from mongodb
  fastify.get("/", async function (req, res) {
    const mongo = db();

    await mongo.connect();
    const result = await mongo
      .db("definitions")
      .collection("DefColl")
      .find({})
      .toArray();
    await mongo.close();

    res.code(200).send(result);
  });

  done();
}
