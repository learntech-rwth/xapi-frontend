import {
  getBasket,
  addBasket,
  removeBasket,
} from "../../controllers/baskets.controller.js";

export default function (fastify, opts, done) {
  fastify.get("/:id", getBasket);
  fastify.post("/", addBasket);
  fastify.delete("/:id", removeBasket);
  done();
}
