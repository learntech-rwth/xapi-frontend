import {
  fetchFolders,
  fetchDefinitions,
} from "../../controllers/definitions.controller.js";

export default function (fastify, opts, done) {
  fastify.get("/folders", fetchFolders);
  fastify.get("/", fetchDefinitions);
  done();
}
