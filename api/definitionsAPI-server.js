"use strict";

import initServer from "./initServer.js";

const app = initServer(process.env.DEF_API_PORT || 2000);

import definitionRoute from "./routes/d-api/definitions.route.js";

app.register(definitionRoute, { prefix: "/d-api" });

async function main() {}

main().catch(console.error);
