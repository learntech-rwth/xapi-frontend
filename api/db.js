import { MongoClient } from "mongodb";

export default function () {
  return new MongoClient(process.env.MONGO_SERVER);
}
