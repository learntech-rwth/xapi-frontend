// @ts-nocheck
import * as dotenv from "dotenv";
dotenv.config();
const {
  MONGO_SERVER,
  DEFINITION_DIRECTORY,
  STRIP_PREFIX,
  DEFINITIONS_URL,
  REPOSITORY_URL,
  REBUILD_REPOSITORY,
} = process.env;

import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import { buildDataBase } from "../../DefinitionService/src/mongo/buildDatabase";

interface Arguments {
  mongoserver: string;
  defdir: string;
  xapiurl: string;
  prefix: string;
  remoterepo: string;
  rebuild: boolean;
  service: boolean;
}

yargs(hideBin(process.argv))
  .scriptName("definition-tools")
  .usage("$0 <cmd> [args]")
  .command({
    command: "build-db",
    description: "builds definition database",
    handler: async (argv: Arguments) => {
      const {
        mongoserver,
        defdir,
        xapiurl,
        prefix,
        remoterepo,
        rebuild,
        service,
      } = argv;
      console.log("buildDataBase for:", argv);
      console.time("buildDataBase");
      buildDataBase(
        defdir,
        mongoserver,
        xapiurl,
        prefix,
        undefined,
        undefined,
        remoterepo,
        rebuild,
        service,
      )
        .then(() => {
          console.timeEnd("buildDataBase");
          console.log(`buildDataBase - Done`);
          if (!service) process.exit();
        })
        .catch((err) => {
          console.log("buildDataBase - Failed");
          console.log(err);
          process.exit();
        });
    },
  })
  .positional("mongoserver", {
    alias: "m",
    type: "string",
    default: MONGO_SERVER || "mongodb://mongodb:27017",
    describe: "connection string to connect to mongodb - env: MONGO_SERVER",
  })
  .positional("defdir", {
    alias: "d",
    type: "string",
    default: DEFINITION_DIRECTORY || "definitions",
    describe:
      "path to directory storing definitions - env: DEFINITION_DIRECTORY",
  })
  .positional("prefix", {
    alias: "p",
    type: "string",
    default: STRIP_PREFIX || "/",
    describe: "prefix to be stripped from XAPI-URL - env: STRIP_PREFIX",
  })
  .positional("xapiurl", {
    alias: "x",
    type: "string",
    default: DEFINITIONS_URL || "http://localhost:3000/definitions/",
    describe: "base-URL used to create IRI for definitions - env: REGISTRY_URL",
  })
  .positional("remoterepo", {
    alias: "r",
    type: "string",
    default: REPOSITORY_URL,
    describe: "URL of definitions repository - env: REPOSITORY_URL",
  })
  .positional("rebuild", {
    alias: "b",
    type: "boolean",
    default: REBUILD_REPOSITORY || false,
    describe:
      "Indicates if the db build should pull new remote repo (false if offline)",
  })
  .positional("service", {
    alias: "s",
    type: "boolean",
    default: REBUILD_REPOSITORY || false,
    describe: "Indicates whether a service for live-rebuild should be started",
  })
  .help().argv;
