module.exports = {
  root: true,
  ecmaFeatures: {
    modules: true,
  },
  env: {
    es6: true,
    commonjs: true,
    jasmine: true,
  },
  plugins: ["@typescript-eslint"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
  ],
  rules: {
    "@typescript-eslint/no-explicit-any": "warn",
    "@typescript-eslint/member-delimiter-style": "off",
    "@typescript-eslint/no-inferrable-types": "warn",
  },
};
