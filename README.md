# xAPI Definitions Registry (xAPI-frontend)

## Einführung & Infrastruktur

Das **xAPI Registry** ist aktuell unter [xapi.elearn.rwth-aachen.de](https://xapi.elearn.rwth-aachen.de/) erreichbar. Eine
Erläuterung der kompletten Infrastruktur ist in
der [GitLab-Wiki](https://gitlab.com/learntech-rwth/xapi-frontend/-/wikis/home) zu finden.

## Build for development

### Docker

Das Projekt kann durch [Docker](https://www.docker.com/) lokal gebuildet und ausgeführt werden. Dazu werden diverse
Dockerfiles und Dockercomposes benutzt.

Zunächst muss eine `.env` Datei aus `.env.dev` angelegt werden. Anschließend kann `docker compose up` im jeweiligen Ordner ausgeführt werden um die jeweils nötigen Container zu builden. Um das komplette Projekt in Docker zu builden kann der folgende Command ausgeführt werden.

```
docker compose -f docker-compose-dev.yml up -d
```

| Ordner                 | Erklärung                                                                                                                                                                                                                                        |
| ---------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **/**                  | Es werden alle benötigten Container erstellt und anschließend kann lokal das Frontend unter `http://localhost:3000/` aufgerufen werden. Dabei werden die Defintionen aus dem Ordner `/DefinitionService/definitions/` in die MongoDB importiert. |
| **/DefinitionService** | Erstellt ausschließlich eine Mongo-Instanz. Kann zum lokalen entwickeln benutzt werden.                                                                                                                                                          |

### npm

Alle Komponenten können auch alternativ durch [npm](https://www.npmjs.com/) gestartet werden. Die zugehörigen npm-Skripte sind in den folgenden
packages zu finden:

| Komponente        | Ordner                           | Standard npm-Skript  |
| ----------------- | -------------------------------- | -------------------- |
| functionsAPI      | /api/functionsAPI/package.json   | `npm run start`      |
| definitionsAPI    | /api/definitionsAPI/package.json | `npm run start`      |
| CLI               | /CLI/package.json                | `npm run repo2mongo` |
| DefinitionService | /DefinitionService/package.json  | `npm run start`      |
| e2e               | /e2e/package.json                | `npm run test`       |
| frontend          | /frontend/package.json           | `npm run dev`        |

## API

Die MongoAPI kann benutzt werden um mit der mongodb zu kommunizieren. Alle im Frontend implementierten Funktionalitäten sind auch über die MongoAPI als API-Anfragen durchführbar. Dazu wurde die MongoAPI in zwei kleinere APIs aufgeteilt. Die **definitionsAPI** ist eine read-only API, welche ungeachtet der Anfrage alle Definitionen zurückgibt. Die **functionsAPI** ist eine REST-API, welche komplexere Anfragen zulässt. Alle API Anfragen sind an `https://xapi.elearn.rwth-aachen.de` zu stellen. Die Anfragen benötigen die jeweiligen Parameter im Body der Request und werden im JSON-Format beantwortet.

### definitionsAPI

| HTTP | URL    | Parameter (Body) | Funktion                                   |
| ---- | ------ | ---------------- | ------------------------------------------ |
| GET  | /d-api | -                | Gibt alle verfügbaren Definitionen zurück. |

### functionsAPI

| HTTP   | URL                            | Parameter (Body)                                                          | Funktion                                                                               |
| ------ | ------------------------------ | ------------------------------------------------------------------------- | -------------------------------------------------------------------------------------- |
| GET    | /f-api<br>/definitions/folders | -                                                                         | Gibt die bestehende Ordnerstruktur aller Unterordnern zurück.                          |
| GET    | /f-api<br>/definitions         | (Query Parameters) def: **Def-IRI** or <br>defs: **Many Def-IRIs**        | Gibt alle Daten der Definition mit der IRI **Def-IRI** zurück.                         |
| GET    | /f-api<br>/baskets/:basketId   |                                                                           | Gibt alle Definitionen des Baskets mit der ID **Basket-ID** zurück.                    |
| POST   | /f-api<br>/baskets             | basket: **Basket-ID**,<br>def: **Def-IRI** or <br>defs: **Many Def-IRIs** | Fügt dem Basket mit der ID **Basket-ID** die Definition mit der IRI **Def-IRI** hinzu. |
| DELETE | /f-api<br>/baskets             | basket: **Basket-ID**,<br>def: **Def-IRI** or <br>defs: **Many Def-IRIs** | Löscht aus dem Basket mit der ID **Basket-ID** die Definition mit der IRI **Def-IRI**. |
